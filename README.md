# STC8Ax核心板

#### 介绍
这是一个基于STC8A8K64S4A12制作的核心板，板载CH340转串口芯片，方便下载和调试。同时引出了ADC外设的引脚，方便个人扩展ADC电路。

##### PCB板：

<img src='doc/image/正面1.jpg'/> <img src='doc/image/反面1.jpg'/>

##### AD渲染图：
<img src='doc/image/AD渲染.jpg'/>

#### 使用说明

1.  推荐使用AD打开本文件
2.  可以使用嘉立创打板验证
3.  如果对你有用，可以给我一个Star！